package ru.grpc.client.dto;


import lombok.Data;

@Data
public class TeacherDTO {
    private Long teacherid;

    private String firstname;

    private String lastname;

    private String middlename;

    private String cathedra;

    @Override
    public String toString() {
        return "{" +
                "\"teacherid\"=\"" + teacherid +
                "\", \"firstname\"=\"" + firstname + "\"" +
                ", \"lastname\"=\"" + lastname + "\"" +
                ", \"middlename\"=\"" + middlename + "\"" +
                ", \"cathedra\"=\"" + cathedra + "\"}";
    }
}
