package ru.grpc.client.dto;

import lombok.Data;

@Data
public class StudentDTO {
    private Long studentid;

    private String firstname;

    private String lastname;

    private String middlename;

    private String specialty;

    private String course;

    @Override
    public String toString() {
        return "{ " +
                "\"studentid\"=\"" + studentid + "\""+
                ", \"firstname\"=\"" + firstname + "\"" +
                ", \"lastname\"=\"" + lastname + "\"" +
                ", \"middlename=\"" + middlename + "\"" +
                ", \"specialty\"=\"" + specialty + "\"" +
                ", \"course\"=\"" + course + "\" }";
    }
}
