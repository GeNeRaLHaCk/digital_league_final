package ru.grpc.client.dto;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentsAndTeachersDTO {
    private List<StudentDTO> students = new ArrayList<>();
    private List<TeacherDTO> teachers = new ArrayList<>();
    private String operation;

    public void addStudent(StudentDTO student){
        students.add(student);
    }
    public void addTeacher(TeacherDTO teacher){
        teachers.add(teacher);
    }

    public void setStudentsFromString(String jsonBody)
            throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        List<StudentDTO> studentList =
                mapper.readValue(jsonBody,
                        new TypeReference<List<StudentDTO>>() {
                        });
        System.out.println("SIZE:" + studentList.size());
        students = studentList;
    }
    public void setTeachersFromString(String jsonBody)
            throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        List<TeacherDTO> teachersList =
                mapper.readValue(jsonBody,
                        new TypeReference<List<TeacherDTO>>() {
                        });
        System.out.println("SIZE: " + teachersList.size());
        teachers = teachersList;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("{ \"operation\":" + "\"" + operation +
                "\", \"students\":[");
        for(int i = 0; i < students.size(); i++){
            if(i == students.size() - 1)
                sb.append(students.get(i).toString());
            else sb.append(students.
                    get(i).toString() + ",");
        }
        sb.append("], \"teachers\":[");
        for(int i = 0; i < teachers.size(); i++){
            if(i == teachers.size() - 1)
                sb.append(teachers.get(i).toString());
            else sb.append(teachers.
                    get(i).toString() + ",");
        }
        sb.append("]}");
        return sb.toString();
    }
}
