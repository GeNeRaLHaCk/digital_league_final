package ru.grpc.client.consumers;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;
import ru.digital_league.core.Log;
import ru.digital_league.core.LogServiceGrpc;
import ru.grpc.client.dto.StudentDTO;
import ru.grpc.client.dto.StudentsAndTeachersDTO;
import ru.grpc.client.dto.TeacherDTO;
import java.util.List;

@Component
@Slf4j
public class ActiveMQConsumer {

    @Autowired
    public ManagedChannel managedChannel;
    @Autowired
    public ModelMapper mapper;
    @JmsListener(destination = "studentQueue")
    @SendTo("studentQueue-answer")
    public String receiveLogStudent(String message)
            throws InterruptedException {
        try {
            Log.TeachersAndStudents studentsGrpc =
                    buildFromJson(message);
            handleReceiveMessage(managedChannel,
                    studentsGrpc);
        } catch (Exception e){
            if(e.getMessage().contains("UNAVAILABLE")) {
                managedChannel.resetConnectBackoff();
                managedChannel = ManagedChannelBuilder.
                        forTarget("grpcserver:6987")
                        .usePlaintext().build();
                Thread.sleep(10000);
                return receiveLogStudent(message);
            }
        }
        //отправляем обратно в другую очередь
        return "Receive by grpcClient message: " + message
                + "\r\n And send to server gRPC";
    }

    @JmsListener(destination = "teacherQueue")
    @SendTo("teacherQueue-answer")
    public String receiveLogTeacher(String message)
            throws InterruptedException {
        try {
            Log.TeachersAndStudents teachersGrpc =
                    buildFromJson(message);
            handleReceiveMessage(managedChannel,
                    teachersGrpc);
        } catch (Exception e){
            if(e.getMessage().contains("UNAVAILABLE")) {
                managedChannel.resetConnectBackoff();
                managedChannel = ManagedChannelBuilder.
                        forTarget("grpcserver:6987")
                        .usePlaintext().build();
                Thread.sleep(10000);
                return receiveLogTeacher(message);
            }
        }
        //отправляем обратно в другую очередь
        return "Receive by grpcClient message: " + message
                + "\r\n And send to server gRPC";
    }
    public void handleReceiveMessage(ManagedChannel
                                               channel,
                                       Log.TeachersAndStudents
                                               generalBuild) {
        //stub - специальный объект на котором мы можем делать
        //удалённые вызовы процедур, делает запрос по сети и вызывает
        //методы log на сервере, получит ответ в responseObserver
        //и вернет ответ сюда, клиенту
        LogServiceGrpc.LogServiceBlockingStub stub =
                LogServiceGrpc.newBlockingStub(channel);
        Log.okResponse response = stub.log(generalBuild);
        System.out.println("RESPONSE from grpc server to grpc client: " +
                response.getPong());
    }
    public Log.TeachersAndStudents buildFromJson(
            String jsonBody) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        StudentsAndTeachersDTO studentsAndTeachers =
                mapper.readValue(jsonBody,
                        StudentsAndTeachersDTO.class);
        List<TeacherDTO> teachers = studentsAndTeachers.getTeachers();
        List<StudentDTO> students = studentsAndTeachers.getStudents();
        Log.TeachersAndStudents.Builder generalBuilder =
                Log.TeachersAndStudents.newBuilder();
        Log.Teacher.Builder builderTeacher = Log.Teacher.newBuilder();
        Log.Student.Builder builderStudent = Log.Student.newBuilder();
        generalBuilder.setOperation(studentsAndTeachers.getOperation());
        if(!teachers.isEmpty())
            for (int i = 0; i < teachers.size(); i++) {
                if (teachers.get(i).getTeacherid() == null){}
                else builderTeacher.setTeacherid(teachers.get(i).getTeacherid());
                builderTeacher.setFirstname(teachers.get(i).getFirstname());
                builderTeacher.setLastname(teachers.get(i).getLastname());
                builderTeacher.setMiddlename(teachers.get(i).getMiddlename());
                builderTeacher.setCathedra(teachers.get(i).getCathedra());
                generalBuilder.addTeachers(builderTeacher.build());
            }
        if(!students.isEmpty())
            for (int i = 0; i < students.size(); i++) {
                if (students.get(i).getStudentid() == null){}
                else builderStudent.setStudentid(students.get(i).getStudentid());
                builderStudent.setFirstname(students.get(i).getFirstname());
                builderStudent.setLastname(students.get(i).getLastname());
                builderStudent.setMiddlename(students.get(i).getMiddlename());
                builderStudent.setSpecialty(students.get(i).getSpecialty());
                builderStudent.setCourse(students.get(i).getCourse());
                generalBuilder.addStudents(builderStudent);
            }
        return generalBuilder.build();
    }
}

