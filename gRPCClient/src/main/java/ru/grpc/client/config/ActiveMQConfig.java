package ru.grpc.client.config;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.jms.annotation.EnableJms;

@Configuration
@EnableAutoConfiguration
@EnableJms
@ComponentScan("ru.grpc.client")
@PropertySource("classpath:application.properties")
//@ImportResource("classpath:activemq.xml")
public class ActiveMQConfig {
    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public ManagedChannel managedChannel(){
        //канал передачи данных, передавать и принимать
        // данные с сервера grpc(localhost)
        ManagedChannel channel = ManagedChannelBuilder.
                forTarget("localhost:6987")
                .usePlaintext().build();
        return channel;
    }
//реализуем конвертер т.к. по умолчанию умеет мало
/*    @Bean
    public MappingJackson2MessageConverter messageConverter(){
        //переприсвоить нельзя
        val messageConverter = new MappingJackson2MessageConverter();
        messageConverter.setTypeIdPropertyName("content-type");
        messageConverter.setTypeIdMappings(
                Collections.singletonMap(
                        "studentEntity",
                        StudentEntity.class));
        return messageConverter;
    }*/
/*    @Bean
    public DefaultJmsListenerContainerFactory
    jmsListenerContainerFactory(
            @Qualifier("jmsConnectionFactory")
                    ConnectionFactory connectionFactory){
        DefaultJmsListenerContainerFactory
                jmsListenerContainerFactory =
                new DefaultJmsListenerContainerFactory();
        jmsListenerContainerFactory.setConnectionFactory(
                connectionFactory);
*//*        jmsListenerContainerFactory.setM
essageConverter(messageConverter());*//*
        jmsListenerContainerFactory.setConcurrency("5-10");
        return jmsListenerContainerFactory;
    }*/
}
