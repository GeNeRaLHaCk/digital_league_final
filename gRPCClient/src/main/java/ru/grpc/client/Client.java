package ru.grpc.client;

import org.springframework.boot.SpringApplication;
import ru.grpc.client.config.ActiveMQConfig;

public class Client {
    public static void main(String[] args) {
        SpringApplication.run(ActiveMQConfig.class);
    }
}

