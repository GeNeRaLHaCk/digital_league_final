package ru.grpc.server;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import java.io.IOException;

public class Main {
    public static void main(String[] args)
            throws IOException, InterruptedException {
        Server server = ServerBuilder.forPort(6987)
                //добавляем все сервисы что нам нужны
                .addService(new LogServiceImpl())
                .build();
        server.start();
        System.out.println("Server started on port: " + server.getPort());
        server.awaitTermination();
    }
}
