package ru.grpc.server;

import io.grpc.stub.StreamObserver;
import ru.digital_league.core.Log;
import ru.digital_league.core.LogServiceGrpc;
import ru.grpc.server.dto.StudentDTO;
import ru.grpc.server.dto.StudentsAndTeachersDTO;
import ru.grpc.server.dto.TeacherDTO;

import java.util.List;

public class LogServiceImpl extends LogServiceGrpc.LogServiceImplBase {
    //т.к. grpc позволяет стримить поток данных
    //в streamObserver мы можем поместить любое количество респонсов(OnNext)
    //на response мы вызываем методы и каждый вызов метода передаёт
    //очередной response на клиента, асинхронный подход

    @Override
    public void log(Log.TeachersAndStudents request,
                    StreamObserver<Log.okResponse> responseObserver) {
        List<Log.Student> listGrpcStudents = request.getStudentsList();
        List<Log.Teacher> listGrpcTeachers = request.getTeachersList();
        StudentsAndTeachersDTO studentsAndTeachers =
                new StudentsAndTeachersDTO();
        if(!listGrpcStudents.isEmpty()) {
            for (int i = 0; i < listGrpcStudents.size(); i++) {
                StudentDTO studentDTO = new StudentDTO();
                studentDTO.setStudentid(listGrpcStudents.get(i).getStudentid());
                studentDTO.setFirstname(listGrpcStudents.get(i).getFirstname());
                studentDTO.setLastname(listGrpcStudents.get(i).getLastname());
                studentDTO.setMiddlename(listGrpcStudents.get(i).getMiddlename());
                studentDTO.setSpecialty(listGrpcStudents.get(i).getSpecialty());
                studentDTO.setCourse(listGrpcStudents.get(i).getCourse());
                studentsAndTeachers.addStudent(studentDTO);
            }
        }
        if(!listGrpcTeachers.isEmpty()){
            for (int i = 0; i < listGrpcTeachers.size(); i++) {
                TeacherDTO teacherDTO = new TeacherDTO();
                teacherDTO.setTeacherid(listGrpcTeachers.get(i).getTeacherid());
                teacherDTO.setFirstname(listGrpcTeachers.get(i).getFirstname());
                teacherDTO.setLastname(listGrpcTeachers.get(i).getLastname());
                teacherDTO.setMiddlename(listGrpcTeachers.get(i).getMiddlename());
                teacherDTO.setCathedra(listGrpcTeachers.get(i).getCathedra());
                studentsAndTeachers.addTeacher(teacherDTO);
            }
        }
        studentsAndTeachers.setOperation(request.getOperation());
        System.out.println("gRPC Server receive: " +
                studentsAndTeachers.toString());
        Log.okResponse response = Log.okResponse.newBuilder()
                //все наши поля определенные в protofile
                .setPong("PONG")
                .build();
        //Для stream
        responseObserver.onNext(response);
        responseObserver.onCompleted();;
    }
}