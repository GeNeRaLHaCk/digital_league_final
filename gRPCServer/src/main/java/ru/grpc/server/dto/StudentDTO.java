package ru.grpc.server.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StudentDTO {
    private Long studentid;

    private String firstname;

    private String lastname;

    private String middlename;

    private String specialty;

    private String course;

    @Override
    public String toString() {
        return "{ " +
                "\"studentid\"=\"" + studentid + "\""+
                ", \"firstname\"=\"" + firstname + "\"" +
                ", \"lastname\"=\"" + lastname + "\"" +
                ", \"middlename=\"" + middlename + "\"" +
                ", \"specialty\"=\"" + specialty + "\"" +
                ", \"course\"=\"" + course + "\" }";
    }
}
