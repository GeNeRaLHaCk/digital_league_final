package ru.grpc.server.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.digital_league.core.Log;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeacherDTO {
    private Long teacherid;

    private String firstname;

    private String lastname;

    private String middlename;

    private String cathedra;

    @Override
    public String toString() {
        return "{" +
                "\"teacherid\"=\"" + teacherid +
                "\", \"firstname\"=\"" + firstname + "\"" +
                ", \"lastname\"=\"" + lastname + "\"" +
                ", \"middlename\"=\"" + middlename + "\"" +
                ", \"cathedra\"=\"" + cathedra + "\"}";
    }
}
