create table privilege
(
    privilegeid   bigint generated by default as identity (maxvalue 2147483647)
        constraint privilege_pkey
            primary key,
    privilegename varchar(255) not null
);