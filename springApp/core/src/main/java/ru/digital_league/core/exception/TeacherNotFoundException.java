package ru.digital_league.core.exception;

public class TeacherNotFoundException extends Exception {
    public TeacherNotFoundException(String message){
        super(message);
    }
}
