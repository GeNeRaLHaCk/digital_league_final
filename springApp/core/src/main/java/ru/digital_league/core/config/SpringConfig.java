package ru.digital_league.core.config;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.sql.DataSource;

@Configuration
@EnableAutoConfiguration
@ComponentScan("ru.digital_league")
@EnableJpaRepositories("ru.digital_league.core.repository")
@EntityScan("ru.digital_league.core.entity")
@ComponentScan("ru.digital_league.core.service")
@ComponentScan("ru.digital_league.api")
@ComponentScan("ru.digital_league.core.controllers")
@ComponentScan("ru.digital_league.core.config")
@EnableWebMvc
//@PropertySource("classpath:application.yml")
@PropertySource("classpath:application.properties")
public class SpringConfig {
    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }
}
