package ru.digital_league.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="studentandteacher")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@AllArgsConstructor
public class StudentAndTeacherEntity {
    @Id
    //@SequenceGenerator(name = "studentIdSeq", sequenceName = "student_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.IDENTITY)//стратегия для создания
    //первичных ключей
    @Column(name = "idstudentandteacher", unique = true)
    private Long idstudentandteacher;

    @JoinColumn(name = "studentid", referencedColumnName = "id")
    //name ассоциация с название таблицы в бд
    @ManyToOne(fetch = FetchType.LAZY)
    private StudentEntity student;

    @JoinColumn(name = "teacherid", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private TeacherEntity teacher;

    public StudentAndTeacherEntity(){}
    public void setId(Long id) {
        this.idstudentandteacher = id;
    }

    public Long getId() {
        return idstudentandteacher;
    }

    public void setStudent(StudentEntity student){
        this.student = student;
    }
    public StudentEntity getStudent(){
        return this.student;
    }
    public void setTeacher(TeacherEntity teacher){
        this.teacher = teacher;
    }
    public TeacherEntity getTeacher(){
        return this.teacher;
    }

    @Override
    public String toString() {
        return "{" +
                "\"student\"=\"" + student +
                "\", \"teacher\"=\"" + teacher +
                "\"}";
    }
}
