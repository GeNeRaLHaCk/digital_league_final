package ru.digital_league.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RoleEntity implements GrantedAuthority {
    private Long roleid;
    private String name;
    @JsonIgnore
    private List<UserEntity> users = new ArrayList<>();
    private List<PrivilegeEntity> privelege = new ArrayList<>();

    public RoleEntity(){}
    public RoleEntity(Long id) {
        this.roleid = id;
    }
    public RoleEntity(String name) {
        this.name = name;
    }
    public RoleEntity(Long id, String name) {
        this.roleid = id;
        this.name = name;
    }

    @Override
    public String getAuthority() {
        return name;
    }
}
