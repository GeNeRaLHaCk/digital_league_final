package ru.digital_league.core.repository.mybatis;

import org.apache.ibatis.annotations.Param;import ru.digital_league.core.entity.Usersroles;

public interface UsersrolesMapper {
    int insert(Usersroles record);

    int insertSelective(Usersroles record);

    /*    int deleteSelective(@Param("userid") Long userid,
                            @Param("roleid") Long roleid);*/
    int insertSelective(@Param("userid") Long userid,
                        @Param("roleid") Long roleid);
}