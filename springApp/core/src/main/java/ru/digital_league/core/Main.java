package ru.digital_league.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.digital_league.core.config.SpringConfig;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(SpringConfig.class, args);
    }
}
