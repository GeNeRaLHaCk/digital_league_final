package ru.digital_league.core.service;

import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.digital_league.core.entity.UserEntity;
import ru.digital_league.core.repository.mybatis.UsersrolesMapper;
import ru.digital_league.core.repository.mybatis.UsertableMapper;

@Service
public class UserService implements UserDetailsService {
    private final UsertableMapper userMapper;
    private final UsersrolesMapper usersrolesMapper;
    @Autowired
    public UserService(UsertableMapper userMapper, UsersrolesMapper usersrolesMapper) {
        this.userMapper = userMapper;
        this.usersrolesMapper = usersrolesMapper;
    }

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {
        System.out.println("FDSSSSSSSSSSSSSSSSSSSSS"+ username);
        UserEntity user = userMapper.selectByUsername(username);
        System.out.println("DDDDDDDDDDDDDDDD SERVICE" + user);
        if (user == null)
            throw new UsernameNotFoundException(
                    "User with that login is not found");
        return new User(user.getUsername(),
                user.getPassword(),
                user.isEnabled(),
                user.isAccountNonExpired(),
                user.isCredentialsNonExpired(),
                user.isAccountNonLocked(),
                user.getAuthorities());
    }

    @Transactional
    public boolean saveStudent(UserEntity user)
            throws PSQLException {
        userMapper.insert(user);
        UserEntity userInDB =
                userMapper.selectByUsername(user.getUsername());
        usersrolesMapper.insertSelective(userInDB.getUserId(), 2L);
        System.out.println("VACHEsdfasdfasdf " + userInDB);
        return true;
        //usersrolesMapper.insertSelective()
    }

}
