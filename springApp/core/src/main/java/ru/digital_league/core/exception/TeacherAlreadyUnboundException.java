package ru.digital_league.core.exception;

public class TeacherAlreadyUnboundException extends Exception {
    public TeacherAlreadyUnboundException(String message){
        super(message);
    }
}
