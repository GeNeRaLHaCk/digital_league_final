package ru.digital_league.core.repository.mybatis;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import ru.digital_league.core.entity.UserEntity;

import java.util.List;

//NON MAGIC LAYER DAO
@Repository
@Mapper
public interface UsertableMapper {
    int deleteByPrimaryKey(Long userid);

    int insert(UserEntity record) throws org.postgresql.util.PSQLException;

    int updateByPrimaryKeySelective(UserEntity record);

    int updateByPrimaryKey(UserEntity record);

    UserEntity selectByUsernameAndPassword(@Param("username") String username,
                                           @Param("password") String password);

    List<UserEntity> selectByUserid(@Param("userid") Long userid);

    UserEntity selectByUsername(@Param("username") String username);
/*
    int updateBatch(List<UserEntity> list);*/
}