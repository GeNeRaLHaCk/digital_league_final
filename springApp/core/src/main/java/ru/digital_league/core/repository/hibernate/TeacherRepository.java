package ru.digital_league.core.repository.hibernate;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.digital_league.core.entity.TeacherEntity;

//Magic layer dao
@Repository
public interface TeacherRepository extends CrudRepository<TeacherEntity,Long> {
    TeacherEntity findByFirstnameAndLastname(String firstname, String lastname);
}
