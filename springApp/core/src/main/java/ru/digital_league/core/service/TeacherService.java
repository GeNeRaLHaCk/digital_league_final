package ru.digital_league.core.service;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.digital_league.core.entity.StudentAndTeacherEntity;
import ru.digital_league.core.entity.StudentEntity;
import ru.digital_league.core.entity.TeacherEntity;
import ru.digital_league.core.exception.TeacherAlreadyExist;
import ru.digital_league.core.exception.TeacherNotFoundException;
import ru.digital_league.core.repository.hibernate.StudentAndTeacherRepository;
import ru.digital_league.core.repository.hibernate.StudentRepository;
import ru.digital_league.core.repository.hibernate.TeacherRepository;
import ru.digital_league.domain.TeacherDTO;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TeacherService {

    private final TeacherRepository teacherRepository;
    private final StudentRepository studentRepository;
    private final StudentAndTeacherRepository studentAndTeacherRepository;
    @Autowired
    public TeacherService(TeacherRepository teacherRepository, StudentRepository studentRepository, StudentAndTeacherRepository studentAndTeacherRepository) {
        this.teacherRepository = teacherRepository;
        this.studentRepository = studentRepository;
        this.studentAndTeacherRepository = studentAndTeacherRepository;
    }

    public TeacherEntity add(TeacherDTO teacher)
            throws TeacherAlreadyExist {
        if(teacherRepository.findByFirstnameAndLastname(
                teacher.getFirstname(),
                teacher.getLastname()) != null)
            throw new TeacherAlreadyExist(
                    "Teacher with that name and surname already exist");
        val teacherEntity = new TeacherEntity();
        teacherEntity.setFirstname(teacher.getFirstname());
        teacherEntity.setLastname(teacher.getLastname());
        teacherEntity.setMiddlename(teacher.getMiddlename());
        teacherEntity.setCathedra(teacher.getCathedra());
        return teacherRepository.save(teacherEntity);
    }

    public TeacherEntity getTeacher(Long id)
            throws TeacherNotFoundException {
        Optional<TeacherEntity> teacher = teacherRepository.findById(id);
        if(!teacher.isPresent())
            throw new TeacherNotFoundException(
                    "Teacher with that id is not found");
        return teacher.get();
    }

    public Long deleteTeacher(Long id) throws TeacherNotFoundException {
        if(!teacherRepository.findById(id).isPresent())
            throw new TeacherNotFoundException(
                    "Teacher with that id is not found");
        teacherRepository.deleteById(id);
        return id;
    }

    public List<TeacherEntity> getTeachers(){//int limit, int page_count){
        ArrayList<TeacherEntity> listTeachers = new ArrayList<>();
        Iterable<TeacherEntity> iterable = teacherRepository.findAll();
        iterable.forEach(listTeachers::add);
        return listTeachers;
    }

    public Iterable<StudentEntity> getListOfStudentsFromTeacher(
            Long teacherid)
            throws TeacherNotFoundException {
        Optional<TeacherEntity> teacher =
                teacherRepository.findById(teacherid);
        if(!teacher.isPresent())
            throw new TeacherNotFoundException(
                    "Teacher with that id is not found");
        //достаём idшники из таблицы manytomany
        List<StudentAndTeacherEntity> listOfRelationships =
                studentAndTeacherRepository.
                        findStudentAndTeacherEntityByTeacher(
                                teacher.get());
        ArrayList<Long> listIdOfStudents = new ArrayList<>();
        listOfRelationships.stream().map(item ->
                item.getStudent().getId())
                .forEach(listIdOfStudents::add);
        //достаём сущности из таблицы student
        Iterable<StudentEntity> iterator = studentRepository.
                findAllById(listIdOfStudents);
        return iterator;
    }
}
