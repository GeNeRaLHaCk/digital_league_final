package ru.digital_league.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class PrivilegeEntity {
    private Long id;
    private String name;
    @JsonIgnore
    private List<RoleEntity> roles = new ArrayList<>();
    public PrivilegeEntity(Long id){
        this.id = id;
    }
    public PrivilegeEntity(Long id, String name){
        this.id = id; this.name = name;
    }
    public PrivilegeEntity(String name){
        this.name = name;
    }
}

