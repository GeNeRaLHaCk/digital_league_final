package ru.digital_league.core.controllers;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;
import ru.digital_league.core.entity.StudentAndTeacherEntity;
import ru.digital_league.core.entity.StudentEntity;
import ru.digital_league.core.entity.TeacherEntity;
import ru.digital_league.core.exception.*;
import ru.digital_league.core.service.StudentService;
import ru.digital_league.domain.StudentDTO;
import ru.digital_league.domain.StudentsAndTeachersDTO;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/students")
public class StudentController {
    private final StudentService studentService;

    private final JmsTemplate jmsTemplate;
    @Autowired
    public StudentController(StudentService studentService,
                             JmsTemplate jmsTemplate) {
        this.studentService = studentService;
        this.jmsTemplate = jmsTemplate;
    }

    @PostMapping(path = "/item")
    public ResponseEntity add(@RequestBody StudentDTO student) {
        try {
            studentService.add(student);
            StudentsAndTeachersDTO studentsAndTeachersDTO =
                    new StudentsAndTeachersDTO();
            studentsAndTeachersDTO.addStudent(student);
            studentsAndTeachersDTO.setOperation("ADD");
            jmsTemplate.convertAndSend("studentQueue",
                    studentsAndTeachersDTO.toString());
            return ResponseEntity.ok("Student is created");
        } catch (StudentAlreadyExist e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping(path = "/item/{id}")
    public ResponseEntity getStudent(@PathVariable Long id) {
        try {
            StudentEntity student = studentService.getStudent(id);
            return ResponseEntity.ok(student);
        } catch (StudentNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/item/{id}")
    public ResponseEntity deleteStudent(@PathVariable Long id) {
        try {
            Long deletedId = studentService.deleteStudent(id);
            StudentsAndTeachersDTO studentsAndTeachersDTO =
                    new StudentsAndTeachersDTO();
            studentsAndTeachersDTO.setOperation("DELETE : " + id);
            jmsTemplate.convertAndSend("studentQueue",
                    studentsAndTeachersDTO.toString());
            return ResponseEntity.ok("Student with id: " +
                    deletedId + " successfully deleted");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/list/items")
    public ResponseEntity getStudents() {
        /*@RequestParam(name = "limit") int limit,
          @RequestParam(name = "page_count") int page_count){*/
        try {
            List<StudentEntity> students = studentService.getStudents();
            StudentsAndTeachersDTO studentsAndTeachersDTO =
                    new StudentsAndTeachersDTO();
            studentsAndTeachersDTO.setOperation("ALLSTUDENT");
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < students.size(); i++){
                if(i == students.size() - 1)
                    sb.append(students.get(i).toString());
                else sb.append(students.get(i).toString() + ",");
            }
            studentsAndTeachersDTO.setStudentsFromString(
                    "[" + sb.toString() + "]");
            jmsTemplate.convertAndSend("studentQueue",
                    studentsAndTeachersDTO.toString());
            return ResponseEntity.ok(students);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/item/bind/{teacherid}/student/{studentid}")
    public ResponseEntity attachStudentToTeacher(
            @PathVariable(name = "teacherid") Long teacherid,
            @PathVariable(name = "studentid") Long studentid) {
        try {
            StudentAndTeacherEntity studentAndTeacher =
                    studentService.attachStudentToTeacher(studentid,
                    teacherid);
            StudentsAndTeachersDTO studentsAndTeachersDTO =
                    new StudentsAndTeachersDTO();
            studentsAndTeachersDTO.setOperation("ATTACH - " +
                    studentAndTeacher.toString());
            jmsTemplate.convertAndSend("studentQueue",
                    studentsAndTeachersDTO.toString());
            return ResponseEntity.ok().body("Student with id " +
                    studentid + " successfull binded to teacher with id " +
                    teacherid);
        }
        catch (StudentNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        catch (TeacherNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        catch (StudentAlreadyAttachedToThisTeacher e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @GetMapping("/item/unbind/{teacherid}/student/{studentid}")
    public ResponseEntity detachStudentFromTeacher(
            @PathVariable(name = "studentid") Long studentid,
            @PathVariable(name = "teacherid") Long teacherid) {
        try {
            studentService.detachStudentFromTeacher(studentid,
                    teacherid);
            StudentsAndTeachersDTO studentsAndTeachersDTO =
                    new StudentsAndTeachersDTO();
            studentsAndTeachersDTO.setOperation("DETACH - " +
                    "{\"student\":\"" + studentid +
                    "\",\"teacher\":\"" + teacherid +
                    "\"}");
            jmsTemplate.convertAndSend("studentQueue",
                    studentsAndTeachersDTO.toString());
            return ResponseEntity.ok().body("Student with id " +
                    studentid +
                    " successfull unbinded from teacher with id " +
                    teacherid);
        }
        catch (StudentNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        catch (StudentAlreadyUnboundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        catch (TeacherNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
        catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @GetMapping("/item/{studentid}/list/teachers")
    public ResponseEntity getListOfTeachersFromStudent(
            @PathVariable(name = "studentid") Long studentid){
        try {
            Iterable<TeacherEntity> iterableOfTeachers =
                    studentService.
                            getListOfTeachersFromStudent(studentid);
            List<TeacherEntity> listOfTeachers = StreamSupport.
                    stream(iterableOfTeachers.spliterator(),
                            false)
                    .collect(Collectors.toList());
            StudentsAndTeachersDTO studentsAndTeachersDTO =
                    new StudentsAndTeachersDTO();
            studentsAndTeachersDTO.setOperation(
                    "ListTeachersFromStudent - " + studentid);
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < listOfTeachers.size(); i++){
                if(i == listOfTeachers.size() - 1)
                    sb.append(listOfTeachers.get(i).toString());
                else sb.append(listOfTeachers.
                        get(i).toString() + ",");
            }
            studentsAndTeachersDTO.setTeachersFromString(
                    "[" + sb.toString() + "]");
            jmsTemplate.convertAndSend("studentQueue",
                    studentsAndTeachersDTO.toString());
            return ResponseEntity.ok(listOfTeachers);
        } catch (StudentNotFoundException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

}
