package ru.digital_league.core.exception;

public class StudentAlreadyExist extends Exception{
    public StudentAlreadyExist(String message){
        super(message);
    }
}
