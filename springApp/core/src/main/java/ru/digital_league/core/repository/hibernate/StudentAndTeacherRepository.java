package ru.digital_league.core.repository.hibernate;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.digital_league.core.entity.StudentAndTeacherEntity;
import ru.digital_league.core.entity.StudentEntity;
import ru.digital_league.core.entity.TeacherEntity;

import java.util.List;

//Magic layer DAO
@Repository
public interface StudentAndTeacherRepository extends CrudRepository<StudentAndTeacherEntity, Long> {
    StudentAndTeacherEntity findByStudentAndTeacher(
            StudentEntity student, TeacherEntity teacher);
    Integer deleteByStudentAndTeacher(
            StudentEntity student, TeacherEntity teacher);
    List<StudentAndTeacherEntity> findStudentAndTeacherEntityByStudent(StudentEntity student);
    List<StudentAndTeacherEntity> findStudentAndTeacherEntityByTeacher(TeacherEntity teacher);
}
