package ru.digital_league.core.controllers;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.User;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.digital_league.api.UserAuthenticationService;
import ru.digital_league.core.service.AuthenticationService;
import ru.digital_league.core.service.UserService;
import ru.digital_league.domain.AccountDTO;
import ru.digital_league.domain.LogInDTO;
import ru.digital_league.domain.RegisterDTO;

import java.security.Principal;

@RestController
public class AuthController {
    @Autowired
    private UserService userService;
    /*    @Autowired
        private JwtUtils jwtProvider;*/
    private final UserAuthenticationService authentication;
    @Autowired
    public AuthController(UserAuthenticationService authentication) {
        this.authentication = authentication;
    }

    @GetMapping("/login/{login}/{password}")
    public ResponseEntity authorize(
            @PathVariable("login") String login,
                      @PathVariable("password") String password)
    {
        if(login == null ||
                password == null)
            return ResponseEntity
                    .badRequest().body(
                            "Login or password is null");
        LogInDTO logInDTO = new LogInDTO(login,password);
        System.out.println(login+password);
        Authentication auth = authentication.authorize(logInDTO);
        return ResponseEntity.ok().body(auth);
    }

    @PostMapping("/register")
    public ResponseEntity register(@RequestBody
                                           RegisterDTO registerDTO){
        try {
            if(registerDTO.getLogin() == null ||
                    registerDTO.getPassword() == null)
                return ResponseEntity.badRequest().body(
                        "Wrong JSON body");
            AccountDTO account =
                    authentication.registerStudent(registerDTO);
            return ResponseEntity.ok().body(account);
        } catch (Exception e) {
            if(e.getMessage().contains("duplicate key value"))
                return ResponseEntity.ok().body(
                        "User with that name already exist");
            return ResponseEntity.ok().body(e.getMessage());
        }
    }
    @GetMapping("/current")//заинжектить Principal текущий пользователь
    public User getCurrent(@AuthenticationPrincipal User user) {
        System.out.println("CURR: " + user);
        return user;
    }

    @GetMapping("/logout")
    public ResponseEntity logout(@AuthenticationPrincipal User user) {
        System.out.println("Logout: " + user);
        authentication.logout();
        return ResponseEntity.ok().body("Successfull logout");
    }
}
