package ru.digital_league.core.controllers;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;
import ru.digital_league.core.entity.StudentEntity;
import ru.digital_league.core.entity.TeacherEntity;
import ru.digital_league.core.exception.TeacherAlreadyExist;
import ru.digital_league.core.exception.TeacherNotFoundException;
import ru.digital_league.core.service.TeacherService;
import ru.digital_league.domain.StudentDTO;
import ru.digital_league.domain.StudentsAndTeachersDTO;
import ru.digital_league.domain.TeacherDTO;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/teachers")
public class TeacherController {
    private final TeacherService teacherService;
    private final JmsTemplate jmsTemplate;
    @Autowired
    public TeacherController(TeacherService teacherService,
                             JmsTemplate jmsTemplate) {
        this.teacherService = teacherService;
        this.jmsTemplate = jmsTemplate;
    }

    @PostMapping(path = "/item")
    public ResponseEntity add(@RequestBody TeacherDTO teacher){
        try {
            teacherService.add(teacher);
            StudentsAndTeachersDTO studentsAndTeachersDTO =
                    new StudentsAndTeachersDTO();
            studentsAndTeachersDTO.addTeacher(teacher);
            studentsAndTeachersDTO.setOperation("ADD");
            jmsTemplate.convertAndSend("teacherQueue",
                    studentsAndTeachersDTO.toString());
            return ResponseEntity.ok("Teacher is created");
        } catch (TeacherAlreadyExist e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @GetMapping(path = "/item/{id}")
    public ResponseEntity getTeacher(@PathVariable Long id){
        try {
            return ResponseEntity.ok(teacherService.getTeacher(id));
        } catch (TeacherNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @DeleteMapping("/item/{id}")
    public ResponseEntity deleteTeacher(@PathVariable Long id){
        try {
            Long deletedId = teacherService.deleteTeacher(id);
            StudentsAndTeachersDTO studentsAndTeachersDTO =
                    new StudentsAndTeachersDTO();
            studentsAndTeachersDTO.setOperation("DELETE : " + id);
            jmsTemplate.convertAndSend("teacherQueue",
                    studentsAndTeachersDTO.toString());
            return ResponseEntity.ok("Teacher with id: " +
                    deletedId + " successfully deleted");
        } catch (TeacherNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @GetMapping("/list/items")
    public ResponseEntity getTeachers(){
        /*@RequestParam(name = "limit") int limit,
        @RequestParam(name = "page_count") int page_count){*/
        try {
            List<TeacherEntity> teachers =
                    teacherService.getTeachers();
            StudentsAndTeachersDTO studentsAndTeachersDTO =
                    new StudentsAndTeachersDTO();
            studentsAndTeachersDTO.setOperation("ALLTEACHER");
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < teachers.size(); i++){
                if(i == teachers.size() - 1)
                    sb.append(teachers.get(i).toString());
                else sb.append(teachers.get(i).toString() + ",");
            }
            studentsAndTeachersDTO.setTeachersFromString(
                    "[" + sb.toString() + "]");
            jmsTemplate.convertAndSend("teacherQueue",
                    studentsAndTeachersDTO.toString());
            return ResponseEntity.ok(teachers);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
    @GetMapping("/item/{teacherid}/students/list")
    public ResponseEntity getListOfStudentsFromTeacher(
            @PathVariable(name = "teacherid") Long teacherid){
        try {
            Iterable<StudentEntity> iterableOfStudents =
                    teacherService.getListOfStudentsFromTeacher(teacherid);
            List<StudentEntity> listOfStudents = StreamSupport.
                    stream(iterableOfStudents.spliterator(),
                            false)
                    .collect(Collectors.toList());
            StudentsAndTeachersDTO studentsAndTeachersDTO =
                    new StudentsAndTeachersDTO();
            studentsAndTeachersDTO.setOperation(
                    "ListStudentsFromTeacher - " + teacherid);
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < listOfStudents.size(); i++){
                if(i == listOfStudents.size() - 1)
                    sb.append(listOfStudents.get(i).toString());
                else sb.append(listOfStudents.
                        get(i).toString() + ",");
            }
            studentsAndTeachersDTO.setStudentsFromString(
                    "[" + sb.toString() + "]");
            jmsTemplate.convertAndSend("teacherQueue",
                    studentsAndTeachersDTO.toString()
            );
            return ResponseEntity.ok(listOfStudents);
        } catch (TeacherNotFoundException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

}
