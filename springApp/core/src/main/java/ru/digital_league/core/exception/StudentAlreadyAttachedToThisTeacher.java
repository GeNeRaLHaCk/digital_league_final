package ru.digital_league.core.exception;

public class StudentAlreadyAttachedToThisTeacher extends Exception{
    public StudentAlreadyAttachedToThisTeacher(String message){
        super(message);
    }
}
