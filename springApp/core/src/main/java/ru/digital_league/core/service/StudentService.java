package ru.digital_league.core.service;

import lombok.val;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.digital_league.core.entity.StudentAndTeacherEntity;
import ru.digital_league.core.entity.StudentEntity;
import ru.digital_league.core.entity.TeacherEntity;
import ru.digital_league.core.exception.*;
import ru.digital_league.core.repository.hibernate.StudentAndTeacherRepository;
import ru.digital_league.core.repository.hibernate.StudentRepository;
import ru.digital_league.core.repository.hibernate.TeacherRepository;
import ru.digital_league.domain.StudentDTO;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    public StudentAndTeacherRepository studentAndTeacherRepository;
    @Autowired
    private TeacherRepository teacherRepository;

    public StudentEntity add(StudentDTO student)
            throws StudentAlreadyExist {
        if(studentRepository.findByFirstnameAndLastname(
                student.getFirstname(),
                student.getLastname()) != null)
            throw new StudentAlreadyExist(
                    "Student with that name and surname already exist");
        val studentEntity = new StudentEntity();
        studentEntity.setFirstname(student.getFirstname());
        studentEntity.setLastname(student.getLastname());
        studentEntity.setMiddlename(student.getMiddlename());
        studentEntity.setSpecialty(student.getSpecialty());
        studentEntity.setCourse(student.getCourse());
        return studentRepository.save(studentEntity);
    }

    public StudentEntity getStudent(Long id) throws StudentNotFoundException {
        Optional<StudentEntity> student = studentRepository.findById(id);
        if(!student.isPresent())
            throw new StudentNotFoundException(
                    "Student with that id is not found");
        return student.get();
    }

    public Long deleteStudent(Long id) throws StudentNotFoundException {
        if(!studentRepository.findById(id).isPresent())
            throw new StudentNotFoundException(
                    "Student with that id is not found");
        studentRepository.deleteById(id);
        return id;
    }

    public List<StudentEntity> getStudents(){//int limit, int page_count){
        ArrayList<StudentEntity> listStudents = new ArrayList<>();
        Iterable<StudentEntity> iterable = studentRepository.findAll();
        iterable.forEach(listStudents::add);
        return listStudents;
    }

    public StudentAndTeacherEntity attachStudentToTeacher(
            Long studentid, Long teacherid)
            throws StudentNotFoundException,
            StudentAlreadyAttachedToThisTeacher,
            TeacherNotFoundException {
        Optional<StudentEntity> student = studentRepository.
                findById(studentid);
        Optional<TeacherEntity> teacher = teacherRepository.
                findById(teacherid);
        if(!student.isPresent())
            throw new StudentNotFoundException(
                    "Student with that id is not found");
        if(!teacher.isPresent())
            throw new TeacherNotFoundException(
                    "Teacher with that id is not found");
        StudentAndTeacherEntity relationship =
                studentAndTeacherRepository.
                        findByStudentAndTeacher(student.get(), teacher.get());
        if(relationship != null)
            throw new StudentAlreadyAttachedToThisTeacher(
                    "Student already attached to this teacher");
        StudentAndTeacherEntity studentAndTeacherEntity =
                new StudentAndTeacherEntity();
        studentAndTeacherEntity.setStudent(student.get());
        studentAndTeacherEntity.setTeacher(teacher.get());
        studentAndTeacherRepository.save(studentAndTeacherEntity);
        return studentAndTeacherEntity;
    }
    @Transactional
    public int detachStudentFromTeacher(Long studentid, Long teacherid)
            throws StudentNotFoundException,
            TeacherNotFoundException, StudentAlreadyUnboundException {
        Optional<StudentEntity> student =
                studentRepository.findById(studentid);
        Optional<TeacherEntity> teacher =
                teacherRepository.findById(teacherid);
        if(!student.isPresent())
            throw new StudentNotFoundException(
                    "Student with that id is not found");
        if(!teacher.isPresent())
            throw new TeacherNotFoundException(
                    "Teacher with that id is not found");
        StudentAndTeacherEntity relationship = studentAndTeacherRepository
                .findByStudentAndTeacher(student.get(), teacher.get());
        if(relationship == null) throw new StudentAlreadyUnboundException(
                "Student already unbind from this teacher");
        studentAndTeacherRepository.
                deleteByStudentAndTeacher(
                        student.get(), teacher.get());
        return 1;
    }
    @Transactional
    public Iterable<TeacherEntity> getListOfTeachersFromStudent(
            Long studentid)
            throws StudentNotFoundException {
        Optional<StudentEntity> student = studentRepository.
                findById(studentid);
        if(!student.isPresent())
            throw new StudentNotFoundException(
                    "Student with that id is not found");
        //достаём idшники из таблицы manytomany
        List<StudentAndTeacherEntity> listOfRelationships =
                studentAndTeacherRepository.
                        findStudentAndTeacherEntityByStudent(student.get());
        ArrayList<Long> listIdOfTeachers = new ArrayList<>();
        listOfRelationships.stream().map(item -> item.getTeacher().getId()).
                forEach(listIdOfTeachers::add);
        //достаём сущности из таблицы student
        Iterable<TeacherEntity> iterator = teacherRepository.
                findAllById(listIdOfTeachers);
        return iterator;
    }

}
