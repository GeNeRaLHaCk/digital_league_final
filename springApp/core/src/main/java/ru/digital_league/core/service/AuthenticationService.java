package ru.digital_league.core.service;

import org.postgresql.util.PSQLException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import ru.digital_league.api.UserAuthenticationService;
import ru.digital_league.core.entity.UserEntity;
import ru.digital_league.domain.AccountDTO;
import ru.digital_league.domain.LogInDTO;
import ru.digital_league.domain.RegisterDTO;

@Service
public class AuthenticationService implements UserAuthenticationService {
    public final UserService userService;
    public final AuthenticationManager authManager;
    private final BCryptPasswordEncoder passwordEncoder;

    public AuthenticationService(AuthenticationManager authManager, BCryptPasswordEncoder passwordEncoder, UserService userService) {
        this.authManager = authManager;
        this.passwordEncoder = passwordEncoder;
        this.userService = userService;
    }

    @Override
    public Authentication authorize(@RequestBody @Validated LogInDTO loginDto)
            throws RuntimeException {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(
                        loginDto.getLogin(),
                        loginDto.getPassword());
        System.out.println("FDSSSSSSSSSSSSSSSSSSSSS"+ loginDto);
        Authentication authentication =
                authManager.authenticate(authenticationToken);
        System.out.println("FDSSSSSSSSSSSSSSSSSSSSS"+
                authentication.getPrincipal());
        SecurityContextHolder.getContext().
                setAuthentication(authentication);
        return authentication;
    }

    //@Secured("ROLE_ADMIN")
    @Override
    public AccountDTO registerStudent(RegisterDTO registerDto)
            throws Exception {
        UserEntity userEntity = createStudent(registerDto);
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setUserid(userEntity.getUserId());
        accountDTO.setUsername(userEntity.getUsername());
        accountDTO.setPassword(userEntity.getPassword());
        accountDTO.setEnabled(userEntity.isEnabled());
        return accountDTO;
    }

    private UserEntity createStudent(RegisterDTO registerDto)
            throws PSQLException {
        UserEntity user = new UserEntity();
        user.setUsername(registerDto.getLogin());
        user.setPassword(passwordEncoder.encode(registerDto.getPassword()));
        user.setRoleStudent();
        userService.saveStudent(user);
        return user;
    }
    @Override
    public void logout() {
        SecurityContextHolder.clearContext();
    }

/*    @Override
    public Optional<UserEntity> findByToken(String token) {
        UserEntity userEntity = userService.
                findUserByUsernameAndPassword("test", "123");
*//*        UserEntity user = new UserEntity();
        usertable.setUserid(usertable.getUserid());
        usertable.setUsername(usertable.getUsername());
        usertable.setPassword(usertable.getPassword());*//*
        return Optional.of(userEntity);
        //userService.findToken(token);
    }*/

}
