package ru.digital_league.core.exception;

public class TeacherAlreadyAttachedToThisStudent extends Exception{
    public TeacherAlreadyAttachedToThisStudent(String message){
        super(message);
    }
}
