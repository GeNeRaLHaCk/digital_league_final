package ru.digital_league.core.exception;

public class StudentAlreadyUnboundException extends Exception {
    public StudentAlreadyUnboundException(String message){
        super(message);
    }
}
