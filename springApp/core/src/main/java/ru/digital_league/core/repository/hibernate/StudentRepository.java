package ru.digital_league.core.repository.hibernate;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.digital_league.core.entity.StudentEntity;

//Magic layer DAO
@Repository
public interface StudentRepository extends CrudRepository<StudentEntity, Long> {
    StudentEntity findByFirstnameAndLastname(String firstname, String lastname);
}
