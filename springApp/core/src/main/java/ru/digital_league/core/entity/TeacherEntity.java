package ru.digital_league.core.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "teacher")
@JsonIgnoreProperties({"hibernateLazyInitializer"})
@NoArgsConstructor
@AllArgsConstructor
public class TeacherEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true)
    private Long teacherid;

    @Column(name = "firstname")
    private String firstname;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "middlename")
    private String middlename;

    @Column(name = "cathedra")
    private String cathedra;

    @OnDelete(action = OnDeleteAction.CASCADE)
    @OneToMany(mappedBy = "teacher", cascade = CascadeType.ALL,
            orphanRemoval = true)//ищет поле teacher
    private Set<StudentAndTeacherEntity> studentAndTeacherEntity;
    public Long getId() {
        return teacherid;
    }

    public void setId(Long id) {
        this.teacherid = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getCathedra() {
        return cathedra;
    }

    public void setCathedra(String cathedra) {
        this.cathedra = cathedra;
    }

    @Override
    public String toString() {
        return "{" +
                "\"teacherid\":\"" + teacherid +
                "\", \"firstname\":\"" + firstname + "\"" +
                ", \"lastname\":\"" + lastname + "\"" +
                ", \"middlename\":\"" + middlename + "\"" +
                ", \"cathedra\":\"" + cathedra + "\"}";
    }
}
