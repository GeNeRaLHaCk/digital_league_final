package ru.digital_league.core.exception;

public class TeacherAlreadyExist extends Exception{
    public TeacherAlreadyExist(String message){
        super(message);
    }
}
