package ru.digital_league.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentDTO {
    public StudentDTO(Long studentid){
        this.studentid = studentid;
    }
    private Long studentid;

    private String firstname;

    private String lastname;

    private String middlename;

    private String specialty;

    private String course;

    @Override
    public String toString() {
        return "{ " +
                "\"studentid\":\"" + studentid + "\""+
                ", \"firstname\":\"" + firstname + "\"" +
                ", \"lastname\":\"" + lastname + "\"" +
                ", \"middlename\":\"" + middlename + "\"" +
                ", \"specialty\":\"" + specialty + "\"" +
                ", \"course\":\"" + course + "\" }";
    }
}
