package ru.digital_league.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

@Data
@AllArgsConstructor
public class AccountDTO {
    public AccountDTO(){}
    private Long userid;
    @NotNull
    private String username;
    @NotNull
    private String password;
    private boolean enabled = false;
}
