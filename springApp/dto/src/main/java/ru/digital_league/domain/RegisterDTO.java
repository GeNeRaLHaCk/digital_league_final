package ru.digital_league.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.jetbrains.annotations.NotNull;

@Data
@AllArgsConstructor
public class RegisterDTO {
    public RegisterDTO() {}
    @NotNull
    private String login;
    @NotNull
    private String password;
}
