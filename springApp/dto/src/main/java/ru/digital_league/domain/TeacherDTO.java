package ru.digital_league.domain;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeacherDTO {
    public TeacherDTO(Long teacherid){
        this.teacherid = teacherid;
    }
    private Long teacherid;

    private String firstname;

    private String lastname;

    private String middlename;

    private String cathedra;

    @Override
    public String toString() {
        return "{" +
                "\"teacherid\":\"" + teacherid +
                "\", \"firstname\":\"" + firstname + "\"" +
                ", \"lastname\":\"" + lastname + "\"" +
                ", \"middlename\":\"" + middlename + "\"" +
                ", \"cathedra\":\"" + cathedra + "\"}";
    }
}
