package ru.digital_league.api;

import org.springframework.boot.autoconfigure.neo4j.Neo4jProperties;
import org.springframework.security.core.Authentication;
import ru.digital_league.domain.AccountDTO;
import ru.digital_league.domain.LogInDTO;
import ru.digital_league.domain.RegisterDTO;

public interface UserAuthenticationService {
    Authentication authorize(LogInDTO loginDto) throws RuntimeException;
    AccountDTO registerStudent(RegisterDTO registerDto) throws Exception;
    void logout();
}
